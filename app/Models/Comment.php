<?php

namespace App\Models;

use App\Models\Film;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\belongsTo;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'comment', 'film_id'
    ];

    /**
     * A comment belongs to a film.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function country(): belongsTo
    {
        return $this->belongsTo(Film::class);
    }
}
