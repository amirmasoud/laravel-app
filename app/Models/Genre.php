<?php

namespace App\Models;

use App\Models\Film;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * A genre belongs to many films.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function films()
    {
        return $this->belongsToMany(Film::class)->withTimestamps();
    }
}
