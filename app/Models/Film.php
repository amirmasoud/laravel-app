<?php

namespace App\Models;

use App\Models\Genre;
use App\Models\Country;
use App\Models\Comment;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use Facades\App\Repositories\FilmRepository;
use Illuminate\Database\Eloquent\Relations\belongsTo;
use Illuminate\Database\Eloquent\Relations\belongsToMany;

class Film extends Model
{
    use HasSlug;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['created_at', 'updated_at', 'release_date'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'release_date', 'rating', 'ticket_price', 'photo', 'country_id'
    ];

    /**
     * A film belongs to a country.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function country(): belongsTo
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * A film belongs to many genres.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function genres(): belongsToMany
    {
        return $this->belongsToMany(Genre::class)->withTimestamps();
    }

    /**
     * A film has many comments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Set the release date value.
     *
     * @param  string  $value
     * @return void
     */
    public function setReleaseDateAttribute(string $value): void
    {
        $this->attributes['release_date'] = FilmRepository::parseReleaseDate($value);
    }

    /**
     * get the release date value.
     *
     * @return string
     */
    public function getReleaseDateAttribute(): string
    {
        return $this->attributes['release_date'];
    }

    /**
     * get the rating value.
     *
     * @return int
     */
    public function getRatingAttribute(): int
    {
        return $this->attributes['rating'];
    }

    /**
     * get the photo value.
     *
     * @return string
     */
    public function getPhotoAttribute(): string
    {
        return asset('storage/app/' . $this->attributes['photo']);
    }

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
}
