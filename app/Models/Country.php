<?php

namespace App\Models;

use App\Models\Film;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * A country has many films.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function films()
    {
        return $this->hasMany(Film::class);
    }
}
