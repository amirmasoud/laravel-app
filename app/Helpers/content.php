<?php

namespace App\Helpers;

use Illuminate\Http\Request;

class Content
{
    /**
     * Get content config.
     *
     * @param  string $name
     * @return string
     */
    protected function config(string $name, string $model = null)
    {
        if ($model) {
            return config('content.model.' . $model . '.' . $name);
        } else {
            return config('content.' . $name);
        }
    }

    /**
     * Get per page value for resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return integer
     */
    protected function perPage(Request $request) : int
    {
        if ($paginate = $request->query($this->config('map.paginate'))) {
            if ($paginate > 0) {
                return $paginate;
            } else {
                return 9999;
            }
        } else {
            return $this->config('default.per_page');
        }
    }
}
