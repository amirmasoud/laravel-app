<?php

namespace App\Repositories;

use Carbon\Carbon;
use App\Models\Genre;
use Illuminate\Http\UploadedFile;

class FilmRepository
{
    /**
     * Path to store photos.
     */
    private const STORE = 'films';

    /**
     * Parse release date value to Carbon.
     *
     * @param  string $date
     * @return Carbon
     */
    public function parseReleaseDate(string $date): Carbon
    {
        return Carbon::parse($date);
    }

    /**
     * Store a photo to disk.
     *
     * @param  UploadedFile $photo
     * @return string
     */
    public function savePhoto(UploadedFile $photo): string
    {
        return $photo->store(self::STORE);
    }

    /**
     * Map genre names to IDs.
     *
     * @param  array  $genres
     * @return array
     */
    public function getGenreIDs(array $genres): array
    {
        $IDs = [];
        foreach ($genres as $genre) {
            $IDs[] = Genre::whereName($genre)->firstOrFail()->id;
        }
        return $IDs;
    }
}
