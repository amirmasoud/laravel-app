<?php

namespace App\Http\Controllers\Film;

use App\Models\Film;
use App\Contracts\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\FilmCollection;
use App\Http\Requests\Film\IndexRequest;
use App\Http\Requests\Film\CreateRequest;
use App\Http\Requests\Film\UpdateRequest;
use App\Http\Resources\Film as FilmResource;
use Facades\App\Repositories\FilmRepository;

class FilmController extends Controller
{
    /**
     * Content Service Provider
     *
     * @var \App\Contracts\Content
     */
    private $content;

    /**
     * Constructor of the class.
     *
     * @param \App\Contracts\Content $content
     */
    public function __construct(Content $content)
    {
        $this->content = $content;
        $this->content->model = Film::with('genres', 'country', 'comments');
    }

    /**
     * Show paginated films.
     *
     * @return FilmCollection
     */
    public function index(IndexRequest $request)
    {
        return new FilmCollection($this->content->index($request));
    }

    /**
     * Create new film.
     *
     * @param  \App\Http\Requests\Film\CreateRequest $request
     * @return FilmResource
     */
    public function store(CreateRequest $request)
    {
        $data = $request->all();
        $data['photo'] = FilmRepository::savePhoto($request->photo);
        $film = Film::create($data);
        $film->genres()->sync(FilmRepository::getGenreIDs($request->genres));

        return new FilmResource($film);
    }

    /**
     * Show a film.
     *
     * @param  \App\Models\Film $film
     * @return FilmResource
     */
    public function show(Film $film)
    {
        return new FilmResource($this->content->show($film)->load('genres', 'country', 'comments'));
    }

    /**
     * Show a film with slug
     *
     * @param  string $film
     * @return FilmResource
     */
    public function slug($slug)
    {
        return new FilmResource(Film::with('genres', 'country', 'comments')->whereSlug($slug)->first());
    }

    /**
     * Show single film for edit.
     *
     * @param  \App\Models\Film $film
     * @return FilmResource
     */
    public function edit(Film $film)
    {
        return new FilmResource($this->content->edit($film)->load('genres', 'country', 'comments'));
    }

    /**
     * Update a film.
     *
     * @param  \App\Http\Requests\Film\UpdateRequest $request
     * @param  \App\Models\Film $film
     * @return void
     */
    public function update(UpdateRequest $request, Film $film)
    {
        $data = $request->all();
        if (!is_null($data['photo'])) {
            $data['photo'] = FilmRepository::savePhoto($request->photo);
        } else {
            unset($data['photo']);
        }
        $result = $film->update($data);
        $film->genres()->sync(FilmRepository::getGenreIDs($request->genres));

        return $result ? response(null, 204) : response(null, 500);
    }

    /**
     * Destroy/delete a film.
     *
     * @param  \App\Models\Film $film
     * @return void
     */
    public function destroy(Film $film)
    {
        return $this->content->destroy($film);
    }
}
