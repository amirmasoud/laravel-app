<?php

namespace App\Http\Controllers\Film;

use App\Models\Genre;
use App\Contracts\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\GenreCollection;
use App\Http\Requests\Genre\CreateRequest;
use App\Http\Requests\Genre\UpdateRequest;
use App\Http\Requests\Genre\SearchRequest;
use App\Http\Resources\Genre as GenreResource;

class GenreController extends Controller
{
    /**
     * Content Service Provider
     *
     * @var \App\Contracts\Content
     */
    private $content;

    /**
     * Constructor of the class.
     *
     * @param \App\Contracts\Content $content
     */
    public function __construct(Content $content)
    {
        $this->content = $content;
        $this->content->model = Genre::query();
    }

    /**
     * Show paginated genres.
     *
     * @return GenreCollection
     */
    public function index()
    {
        return new GenreCollection($this->content->index());
    }

    /**
     * Create new genre.
     *
     * @param  \App\Http\Requests\Genre\CreateRequest $request
     * @return GenreResource
     */
    public function store(CreateRequest $request)
    {
        return new GenreResource($this->content->store($request));
    }

    /**
     * Show a genre.
     *
     * @param  \App\Models\Genre $genre
     * @return GenreResource
     */
    public function show(Genre $genre)
    {
        return new GenreResource($this->content->show($genre));
    }

    /**
     * Show single genre for edit.
     *
     * @param  \App\Models\Genre $genre
     * @return GenreResource
     */
    public function edit(Genre $genre)
    {
        return new GenreResource($this->content->edit($genre));
    }

    /**
     * Update a genre.
     *
     * @param  \App\Http\Requests\Genre\UpdateRequest $request
     * @param  \App\Models\Genre $genre
     * @return void
     */
    public function update(UpdateRequest $request, Genre $genre)
    {
        return $this->content->update($request, $genre);
    }

    /**
     * Destroy/delete a genre.
     *
     * @param  \App\Models\Genre $genre
     * @return void
     */
    public function destroy(Genre $genre)
    {
        return $this->content->destroy($genre);
    }

    /**
     * Search genres with name.
     *
     * @param  Request $request
     * @return GenreCollection
     */
    public function search(SearchRequest $request)
    {
        return new GenreCollection($this->content->search($request));
    }
}
