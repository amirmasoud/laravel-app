<?php

namespace App\Http\Controllers\Film;

use App\Models\Country;
use App\Contracts\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CountryCollection;
use App\Http\Requests\Country\CreateRequest;
use App\Http\Requests\Country\UpdateRequest;
use App\Http\Requests\Country\SearchRequest;
use App\Http\Resources\Country as CountryResource;

class CountryController extends Controller
{
    /**
     * Content Service Provider
     *
     * @var \App\Contracts\Content
     */
    private $content;

    /**
     * Constructor of the class.
     *
     * @param \App\Contracts\Content $content
     */
    public function __construct(Content $content)
    {
        $this->content = $content;
        $this->content->model = Country::query();
    }

    /**
     * Show paginated countries.
     *
     * @return CountryCollection
     */
    public function index()
    {
        return new CountryCollection($this->content->index());
    }

    /**
     * Create new country.
     *
     * @param  \App\Http\Requests\Country\CreateRequest $request
     * @return CountryResource
     */
    public function store(CreateRequest $request)
    {
        return new CountryResource($this->content->store($request));
    }

    /**
     * Show a country.
     *
     * @param  \App\Models\Country $country
     * @return CountryResource
     */
    public function show(Country $country)
    {
        return new CountryResource($this->content->show($country));
    }

    /**
     * Show single country for edit.
     *
     * @param  \App\Models\Country $country
     * @return CountryResource
     */
    public function edit(Country $country)
    {
        return new CountryResource($this->content->edit($country));
    }

    /**
     * Update a country.
     *
     * @param  \App\Http\Requests\Country\UpdateRequest $request
     * @param  \App\Models\Country $country
     * @return void
     */
    public function update(UpdateRequest $request, Country $country)
    {
        return $this->content->update($request, $country);
    }

    /**
     * Destroy/delete a country.
     *
     * @param  \App\Models\Country $country
     * @return void
     */
    public function destroy(Country $country)
    {
        return $this->content->destroy($country);
    }

    /**
     * Search countries with name.
     *
     * @param  Request $request
     * @return CountryCollection
     */
    public function search(SearchRequest $request)
    {
        return new CountryCollection($this->content->search($request));
    }
}
