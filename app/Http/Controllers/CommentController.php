<?php

namespace App\Http\Controllers;

use App\Models\Film;
use Illuminate\Http\Request;
use App\Http\Requests\Comment\CreateRequest;
use App\Http\Resources\Comment as CommentResource;

class CommentController extends Controller
{
    /**
     * Create new comment for given film.
     *
     * @param  CreateRequest $request
     * @param  Film $film
     * @return
     */
    public function store(CreateRequest $request, Film $film)
    {
        return new CommentResource($film->comments()->create($request->all()));
    }
}
