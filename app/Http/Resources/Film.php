<?php

namespace App\Http\Resources;

use App\Http\Resources\Genre as GenreResource;
use App\Http\Resources\Country as CountryResource;
use App\Http\Resources\Comment as CommentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Film extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'release_date' => $this->release_date,
            'rating' => $this->rating,
            'ticket_price' => $this->ticket_price,
            'photo' => $this->photo,
            'genres' => GenreResource::collection($this->whenLoaded('genres')),
            'comments' => CommentResource::collection($this->whenLoaded('comments')),
            'country' => new CountryResource($this->whenLoaded('country')),
        ];
    }
}
