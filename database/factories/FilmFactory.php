<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Film::class, function (Faker $faker) {
    $country = factory(App\Models\Country::class)->create();
    $genre = factory(App\Models\Genre::class)->create();

    return [
        'name' => $faker->unique()->name,
        'description' => $faker->paragraph,
        'release_date' => '2016-09-11T18:49:00.000Z',
        'rating' => $faker->numberBetween(1, 5),
        'ticket_price' => $faker->numberBetween(0, 35),
        'photo' => $faker->imageUrl,
        'country_id' => $country->id,
    ];
});
