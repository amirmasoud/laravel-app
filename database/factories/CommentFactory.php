<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Comment::class, function (Faker $faker) {
    $film = factory(App\Models\Film::class)->create();

    return [
        'name' => $faker->name,
        'comment' => $faker->paragraph,
        'film_id' => $film->id
    ];
});
