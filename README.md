### Installation
1. Clone the application
2. `composer install`
3. `php artisan key:generate` and `php artisan jwt:secret`
4. `php artisan migrate --seed`
5. `yarn install`
