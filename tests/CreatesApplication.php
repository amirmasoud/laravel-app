<?php

namespace Tests;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Console\Kernel;

trait CreatesApplication
{
    /**
     * @var \App\User
     */
    protected $user;

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        Hash::setRounds(4);

        \Artisan::call('migrate');
        \Artisan::call('db:seed');
        $this->user = factory(User::class)->create();

        return $app;
    }
}
