<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Country;
use Tests\Traits\Resource;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Interfaces\Resource as ResourceInterface;

class CountryTest extends TestCase implements ResourceInterface
{
    use Resource;

    /**
     * @var \App\Models\Country
     */
    protected $country;

    /**
     * Single JSON response structure.
     */
    public const SINGLE_STRUCTURE = [
        'data' => [
            'id', 'name'
        ]
    ];

    /**
     * Base API URL.
     */
    public const BASE_URL = '/api/films-taxonomy/countries/';

    /**
     * Table to check (non)existance of data after create, update and delete.
     */
    public const TABLE = 'countries';

    public function setUp()
    {
        parent::setUp();

        $this->country = factory(Country::class)->create();
    }

    /**
     * @group countries
     */
    public function testSearchCountries()
    {
        $this->search(self::BASE_URL . 'search?q=Afghanistan');
    }

    /**
     * @group countries
     */
    public function testPaginatedCountries()
    {
        $this->paginated(self::BASE_URL);
    }

    /**
     * @group countries
     */
    public function testSingleCountry()
    {
        $this->single(self::BASE_URL . $this->country->id);
    }

    /**
     * @group countries
     */
    public function testCreateCountry()
    {
        $data = [
            'name' => 'country_name'
        ];
        $this->create(self::BASE_URL, $data, self::TABLE);
    }

    /**
     * @group countries
     */
    public function testEditCountry()
    {
        $this->edit(self::BASE_URL . $this->country->id . '/edit');
    }

    /**
     * @group countries
     */
    public function testUpdateCountry()
    {
        $data = [
            'name' => 'update_country_name'
        ];

        $this->update(self::BASE_URL . $this->country->id, $data, self::TABLE);
    }

    /**
     * @group countries
     */
    public function testDestroyCountry()
    {
        $data = [
            'id' => $this->country->id
        ];

        $this->destroy(self::BASE_URL . $this->country->id, $data, self::TABLE);
    }
}
