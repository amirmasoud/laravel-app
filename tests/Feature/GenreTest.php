<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Genre;
use Tests\Traits\Resource;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Interfaces\Resource as ResourceInterface;

class GenreTest extends TestCase implements ResourceInterface
{
    use Resource;

    /**
     * @var \App\Models\Genre
     */
    protected $genre;

    /**
     * Single JSON response structure.
     */
    public const SINGLE_STRUCTURE = [
        'data' => [
            'id', 'name'
        ]
    ];

    /**
     * Base API URL.
     */
    public const BASE_URL = '/api/films-taxonomy/genres/';

    /**
     * Table to check (non)existance of data after create, update and delete.
     */
    public const TABLE = 'genres';

    public function setUp()
    {
        parent::setUp();

        $this->genre = factory(Genre::class)->create();
    }

    /**
     * @group genres
     */
    public function testSearchGenres()
    {
        $this->search(self::BASE_URL . 'search?q=Action');
    }

    /**
     * @group genres
     */
    public function testPaginatedGenres()
    {
        $this->paginated(self::BASE_URL);
    }

    /**
     * @group genres
     */
    public function testSingleGenre()
    {
        $this->single(self::BASE_URL . $this->genre->id);
    }

    /**
     * @group genres
     */
    public function testCreateGenre()
    {
        $data = [
            'name' => 'genre_name'
        ];
        $this->create(self::BASE_URL, $data, self::TABLE);
    }

    /**
     * @group genres
     */
    public function testEditGenre()
    {
        $this->edit(self::BASE_URL . $this->genre->id . '/edit');
    }

    /**
     * @group genres
     */
    public function testUpdateGenre()
    {
        $data = [
            'name' => 'update_genre_name'
        ];

        $this->update(self::BASE_URL . $this->genre->id, $data, self::TABLE);
    }

    /**
     * @group genres
     */
    public function testDestroyGenre()
    {
        $data = [
            'id' => $this->genre->id
        ];

        $this->destroy(self::BASE_URL . $this->genre->id, $data, self::TABLE);
    }
}
