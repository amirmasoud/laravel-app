<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Film;
use App\Models\Genre;
use App\Models\Country;
use Tests\Traits\Resource;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Interfaces\Resource as ResourceInterface;

class FilmTest extends TestCase implements ResourceInterface
{
    use Resource;

    /**
     * @var \App\Models\Film
     */
    protected $film;

    /**
     * @var \App\Models\Country
     */
    protected $country;

    /**
     * @var \App\Models\Genre
     */
    protected $genre;

    /**
     * Single JSON response structure.
     */
    public const SINGLE_STRUCTURE = [
        'data' => [
            'id', 'name', 'description', 'release_date', 'rating', 'ticket_price', 'photo', 'country', 'genres'
        ]
    ];

    /**
     * Base API URL.
     */
    public const BASE_URL = '/api/films/';

    /**
     * Table to check (non)existance of data after create, update and delete.
     */
    public const TABLE = 'films';

    public function setUp()
    {
        parent::setUp();

        $this->film = factory(Film::class)->create();
        $this->country = factory(Country::class)->create();
        $this->genre = factory(Genre::class)->create();
    }

    /**
     * @group films
     */
    public function testPaginatedFilms()
    {
        $this->paginated(self::BASE_URL);
    }

    /**
     * @group films
     */
    public function testSingleFilm()
    {
        $this->single(self::BASE_URL . $this->film->id);
    }

    /**
     * @group films
     */
    public function testCreateFilm()
    {
        Storage::fake('local');
        $file = UploadedFile::fake()->image('photo.jpg');
        $data = [
            'name' => 'film_name',
            'description' => 'sampel description',
            'release_date' => '2018-09-11T18:49:00.000Z',
            'rating' => 4,
            'ticket_price' => '14.99',
            'photo' => $file,
            'country_id' => $this->country->id,
            'genres' => [$this->genre->name]
        ];

        $this->create(self::BASE_URL, $data, self::TABLE);
        Storage::disk('local')->assertExists('films/' . $file->hashName());
    }

    /**
     * @group films
     */
    public function testEditFilm()
    {
        $this->edit(self::BASE_URL . $this->film->id . '/edit');
    }

    /**
     * @group films
     */
    public function testUpdateFilm()
    {
        Storage::fake('local');
        $file = UploadedFile::fake()->image('photo.jpg');
        $data = [
            'name' => 'UPDATED film_name',
            'description' => 'UPDATED sampel description',
            'release_date' => '2020-09-11T18:49:00.000Z',
            'rating' => 4,
            'ticket_price' => '29.99',
            'photo' => $file,
            'country_id' => $this->country->id,
            'genres' => [$this->genre->name]
        ];

        $this->update(self::BASE_URL . $this->film->id, $data, self::TABLE);
        Storage::disk('local')->assertExists('films/' . $file->hashName());
    }

    /**
     * @group films
     */
    public function testDestroyFilm()
    {
        $data = [
            'id' => $this->film->id
        ];

        $this->destroy(self::BASE_URL . $this->film->id, $data, self::TABLE);
    }
}
