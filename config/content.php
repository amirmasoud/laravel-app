<?php

return [
    'map' => [
        'filter' => [
            // Param
            'name' => [
                'column' => 'name'
            ]
        ],
        'order' => [
            'sort'
        ],
        'paginate' => 'per_page',
    ],
    'search' => [
        'columns' => [
            'name',
        ],
        'clause' => 'LIKE'
    ],
    'default' => [
        'per_page' => 15,
    ],
];
