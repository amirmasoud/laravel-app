<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('/films', 'Film\FilmController')->only(['show', 'index'])->parameters('films');
Route::get('/films/{slug}', 'Film\FilmController@slug')->name('films.slug');
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/user', 'UserController@current');

    Route::resource('/films', 'Film\FilmController')->except(['create', 'index', 'show']);
    Route::group(['namespace' => 'Film', 'prefix' => 'films-taxonomy'], function () {
        Route::get('/genres/search', 'GenreController@search')->name('genres.search');
        Route::resource('/genres', 'GenreController')->except(['create']);

        Route::get('/countries/search', 'CountryController@search')->name('countries.search');
        Route::resource('/countries', 'CountryController')->except(['create']);

    });
    Route::post('comments/{film}', 'CommentController@store')->name('comments.store');

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
});
