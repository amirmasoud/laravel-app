const Welcome = () => import('~/pages/welcome').then(m => m.default || m)
const Login = () => import('~/pages/auth/login').then(m => m.default || m)
const Register = () => import('~/pages/auth/register').then(m => m.default || m)
const PasswordEmail = () => import('~/pages/auth/password/email').then(m => m.default || m)
const PasswordReset = () => import('~/pages/auth/password/reset').then(m => m.default || m)
const NotFound = () => import('~/pages/errors/404').then(m => m.default || m)

const Home = () => import('~/pages/home').then(m => m.default || m)
const Settings = () => import('~/pages/settings/index').then(m => m.default || m)
const SettingsProfile = () => import('~/pages/settings/profile').then(m => m.default || m)
const SettingsPassword = () => import('~/pages/settings/password').then(m => m.default || m)

const Film = () => import('~/pages/films/index').then(m => m.default || m)
const FilmBrowse = () => import('~/pages/films/browse').then(m => m.default || m)
const FilmSingle = () => import('~/pages/films/single').then(m => m.default || m)
const ContentBrowse = () => import('~/pages/content/browse').then(m => m.default || m)
const ContentShow = () => import('~/pages/content/show').then(m => m.default || m)
const ContentCreate = () => import('~/pages/content/create').then(m => m.default || m)
const ContentEdit = () => import('~/pages/content/edit').then(m => m.default || m)

export default [
  { path: '/', name: 'welcome', component: Welcome },

  { path: '/login', name: 'login', component: Login },
  { path: '/register', name: 'register', component: Register },
  { path: '/password/reset', name: 'password.request', component: PasswordEmail },
  { path: '/password/reset/:token', name: 'password.reset', component: PasswordReset },

  { path: '/home', name: 'home', component: Home },
  { path: '/settings',
    component: Settings,
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: SettingsProfile },
      { path: 'password', name: 'settings.password', component: SettingsPassword }
    ] },

  { path: '/films',
    component: Film,
    children: [
      { path: '/', name: 'films.browse', component: FilmBrowse },
      { path: '/f/:slug', name: 'films.single', component: FilmSingle },
      { path: ':content', name: 'content.browse', component: ContentBrowse },
      { path: ':content/create', name: 'content.create', component: ContentCreate },
      { path: ':content/:resource', name: 'content.show', component: ContentShow },
      { path: ':content/:resource/edit', name: 'content.edit', component: ContentEdit }
    ]
  },

  { path: '*', component: NotFound }
]
